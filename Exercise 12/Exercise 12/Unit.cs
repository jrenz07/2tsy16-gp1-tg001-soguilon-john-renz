﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_12
{
    class Unit
    {
        public string Name { get; set; }

        public float Damage { get; set; }

        public float Mana { get; set; }

        public float Armor { get; set; }

        public float Health { get; set; }
        public Unit(string name,float health ,float damage,float mana,float armor)
        {
            Name = name;
            Health = health;
            Damage = damage;
            Mana = mana;
            Armor = armor;

        }


    }
}
