﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise9
{
    class Monster
    {
        // constructor

        public Monster(string name, float hp)
        {
            Name = name;
            CurrentHp = hp;
            MaximumHp = hp;
        
        }

        public string Name;
        //make CurrentHp public, and use a getter/setter
        public float CurrentHp;
        //make maximumHp public, and use a getter/setter
        public float MaximumHp;

        void SomeFunction()
        {
            CurrentHp = 10.0f;
        }

    }
}
