﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise9
{
    class Character
    {
        // constructor

        public Character(string name, float hp, float xp)
        {
            Name = name;
            CurrentHp = hp;
            MaximumHp = hp;
            exp = xp;
        }

        public string Name;
        //make CurrentHp public, and use a getter/setter
        public float CurrentHp;
        //make maximumHp public, and use a getter/setter
        public float MaximumHp;
        private float exp;

        public float Exp
        {
            get
            {
                return exp;
            }
            set
            {
                exp = value;
                if (exp < 0)
                    exp = 0;
            }
        }
        void SomeFunction()
        {
            CurrentHp = 10.0f;
        }

    }
}
