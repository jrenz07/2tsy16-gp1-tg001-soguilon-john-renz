﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] items = { "Health Potion", "Mana Potion", "Yggdrasil Leaf", "Dreadwyrm Staff", "Holy Robe" };

            Console.WriteLine("What item do you want to find?");
            string input = Console.ReadLine();

            bool found = false;
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == input)
                {
                    found = true;
                    break;
                }
                if (found)
                {
                    Console.WriteLine("Found");
                }
                else
                {
                    Console.WriteLine("Not found");
                    break;
                }
                Console.ReadLine();
            }
            Console.ReadKey();
        }
    }
}
