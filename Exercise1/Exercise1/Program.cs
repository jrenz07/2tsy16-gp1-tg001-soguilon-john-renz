﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] items = { "Health Potion", "Mana Potion", "Yggdrasil Leaf", "Dreadwyrm Staff", "Holy Robe" };
            Console.WriteLine("Hello There!");

            foreach (string item in items) //this is the new code for loop
            {
                Console.WriteLine(item);
            }

            //for (int i = 0; i < items.Length; i++)
            //{
            //Console.WriteLine(items[i]);
            //}
        }
    }
}
