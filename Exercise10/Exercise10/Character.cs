﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPObjects
{
    public class Character
    {
        public string Name { get; set; }

        private Weapon _weapon;
        public Weapon Weapon
        {
            get; { return _weapon; }
            set
            {
                if (value.Name == "Bow") return;
                _weapon = value;
            }
        }


        //public void EquipWeapon(Weapon weapon)
        //{
        //    if (weapon.Name == "Bow") return;
        //    Weapon = weapon;
        //}
    }
}
