﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_3
{
    class Program
    {
        static bool SearchArray(string[] items, string itemToSearch)
        { 
            bool found = false;
            for (int i = 0; i < items.Length; i++)
            {
                    if (items[i] == itemToSearch)
                    {
                        found = true;
                        break;
                    
                    }
            }
            return found;
        }
        
        
        static void Main(string[] args)
        {

            string[] items = { "Health Potion", "Mana Potion", "Yggdrasil Leaf", "Dreadwyrm Staff", "Holy Robe" };



            Console.WriteLine("What item do you want to find?\n");
            string input = Console.ReadLine();
            bool found = SearchArray(items, input);


            if (found)
            {
                Console.WriteLine("Found");
            }
            else
            {
                Console.WriteLine("Not Found");
            }
            Console.ReadKey(); //sir i don't know what to do next po...
        }
    }
}
